/** @format */

import {AppRegistry,Dimensions,YellowBox} from 'react-native';
import RootStack from './src/routes/RootStack';
import {name as appName} from './app.json';
import SideMenu from './src/componants/SideBar'
import { createDrawerNavigator } from 'react-navigation';
//console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
//YellowBox.ignoreWarnings(['Warning: Each', 'Warning: Failed']);
console.disableYellowBox = true;

const App = createDrawerNavigator({
    Item1: {
        screen: RootStack,
      }
    }, {
      contentComponent: SideMenu,
      drawerWidth: Dimensions.get('window').width - 60,  
  });
AppRegistry.registerComponent(appName, () => App);

