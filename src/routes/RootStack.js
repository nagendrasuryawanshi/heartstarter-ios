import { createStackNavigator } from "react-navigation";
import HomeScreen from "../componants/HomeScreen";
// import SiderBar from "../componants/SideBar";
 import Profile from "../componants/Profile";
// import { FirstHeader , SecondHeader } from '../componants/CommonHeader';
import React ,{ Component } from 'react';
import MapFinder from "../pages/MapFinder";
import { FirstHeader , SecondHeader } from '../componants/CommonHeader';
import CPR from "../pages/CPR_Guide";
import CPR_Detail from "../pages/CPR_Detail";
import Admin from '../pages/Adminviewpage';
import SignUp from "../pages/Signup";
import ActiveOnMap from "../pages/AvailableOnMap";
import AddDevice from "../pages/Add_Device";
import AddDeviceMap from "../pages/Add_DeviceMap";
import AddMoreDevice from "../pages/Add_More_Device";


const RootStack = createStackNavigator({
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        header: null

      })
    },
    Profile: {
      screen: Profile,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    MapFinder: {
      screen: MapFinder,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    AddDevice: {
      screen: AddDevice,
      navigationOptions: ({ navigation }) => ({
        header:null
      })
    },
    AddMoreDevice: {
      screen: AddMoreDevice,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />,
        header: null
      })
    },
    CPR: {
      screen: CPR,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    CPR_Detail: {
      screen: CPR_Detail,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    Admin: {
      screen: Admin,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    ActiveOnMap: {
      screen: ActiveOnMap,
      navigationOptions: ({ navigation }) => ({
        header:null
      })
    },
    AddDeviceMap: {
      screen: AddDeviceMap,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
        header:null
      })
    },
  },
  {
    initialRouteName: 'Home'
  }
  );


  export default RootStack;
