/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  View,
  Linking,
  Alert,
  Image,
  TouchableOpacity,
  AsyncStorage,
  ImageBackgroud,
  Animated
} from "react-native";
import { Container, Button, Icon, Content, Card, Row, Col } from "native-base";
import API from "../api/Apis";
import VersionCheck from "react-native-version-check";
import styles from "../assets/styles/HomeScreenCSS";
import DeviceInfo from "react-native-device-info";
import FooterPage from "./FooterPage";
import {FirstHeader} from "./CommonHeader"
import RNImmediatePhoneCall from "react-native-immediate-phone-call";
import * as Animatable from 'react-native-animatable';

const Apis = new API();
//const getCurrentVersion = VersionCheck.getCurrentVersion();
//const getLatestVersion = VersionCheck.getLatestVersion();

// VersionCheck.needUpdate({
//   currentVersion: `${getCurrentVersion}`,
//   latestVersion: `${getLatestVersion}`
// }).then(res => {
//   console.log(res.isNeeded); // true
//   if (res.isNeeded) {
//     Alert.alert(
//       "Alert Message",
//       "Update is Available",
//       [
//         {
//           text: "Ask me later",
//           onPress: () => console.log("Ask me later pressed")
//         },
//         {
//           text: "Cancel",
//           onPress: () => console.log("Cancel Pressed"),
//           style: "cancel"
//         },
//         {
//           text: "OK",
//           onPress: () => Linking.openURL(VersionCheck.getStoreUrl())
//         }
//       ],
//       { cancelable: false }
//     );
//     // open store if update is needed.
//   }
// });

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: [],
      uid: []
    };
  }

  componentWillMount() {
    Apis.locationPemision();
    const brand = DeviceInfo.getBrand();
    const model = DeviceInfo.getModel();
    //  const phoneNumber = DeviceInfo.getPhoneNumber();
    //  console.log("phoneNumber",phoneNumber);
    const serialNumber = DeviceInfo.getSerialNumber();
    console.log("serialNumber",serialNumber);
    const deviceId = DeviceInfo.getUniqueID();
    console.log("deviceId",deviceId);

    navigator.geolocation.getCurrentPosition(position => {
      console.log(
        `Got location: ${position.coords.latitude}, ${
          position.coords.longitude
        }`
      );
      var location = [];
      location.push(position.coords.latitude);
      location.push(position.coords.longitude);

      var data = {
        deviceId: deviceId,
        location: location,
        serial: serialNumber,
        brand: brand,
        model: model
      };
      console.log("data1", data);

      Apis.getUserDeviceInfo(deviceId).then(res => {
        console.log("getUserDeviceInfo", res);
        var userdevise = res;

        if (userdevise && userdevise.length > 0) {
          console.log("userdevice1", userdevise[0].deviceId);
          if (userdevise[0].deviceId == deviceId) {
            console.log("device id ");

            console.log("already registered");
          } else {
            console.log("device id not available");

            Apis.appInfoSave(data).then(
              res => {
                console.log("result", res);
                //console.log("result",  Date(result));
              },
              error => {
                console.log("error", error);
              }
            );
          }
        } else {
          console.log("userdevice not data");

          Apis.appInfoSave(data).then(
            res => {
              console.log("result", res);
            },
            error => {
              console.log("error", error);
            }
          );
        }
      });
    });
  }

  OnCall() {
    AsyncStorage.getItem("USER", (err, result) => {
      console.log(result);
      navigator.geolocation.getCurrentPosition(position => {
        console.log(
          `Got location: ${position.coords.latitude}, ${
            position.coords.longitude
          }`
        );
        var location = [];
        location.push(position.coords.latitude);
        location.push(position.coords.longitude);

        if (JSON.parse(result).User && JSON.parse(result).User.message) {
          const data = {
            phoneNumber: DeviceInfo.getPhoneNumber(),
            requestTime: new Date(),
            deviceId: DeviceInfo.getUniqueID()
          };
          setTimeout(() => {
            console.log("Open");
            this.props.navigation.navigate("MapFinder", { Data: data });
          }, 3000);
        }

        const data = {
          phoneNumber: JSON.parse(result).User.phoneNumber,
          uid: JSON.parse(result).User._id,
          requestTime: new Date(),
          deviceId: JSON.parse(result).User.deviceId,
          location: location
        };
        setTimeout(() => {
          console.log("Open");
          this.props.navigation.navigate("MapFinder", { Data: data });
        }, 3000);
      });
    });
  }



   
  render() {
    return (
      <Container style={styles.container}>
      <FirstHeader {...this.props} />
        {/* <Content> */}
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1
          }}
        >
          <TouchableOpacity onLongPress={() => this.OnCall()}>
            {/* <View style={{}}> */}
             
            <Animatable.Image animation="pulse" easing="ease-out" iterationCount="infinite"
              source={require("../assets/Images/sos.png")}
              style={styles.image}
            />
            {/* </View> */}
          </TouchableOpacity>
        </View>
        {/* </Content> */}
        <FooterPage {...this.props} />
      </Container>
    );
  }
}
