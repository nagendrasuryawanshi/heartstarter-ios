import React, { Component } from "react";
import { View, StyleSheet, Text,Image } from "react-native";
import { Button, Icon, Row, Col, Card, Footer } from "native-base";
// import { getRhumbLineBearing } from "geolib";


//import styles from "../assets/styles/AvailableOnMapCSS";

export default class FooterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentWillMount() {
    console.log("this.props.navigation.state.routeName", this.props.navigation.state.routeName)
    this.setState({ active: this.props.navigation.state.routeName });
  }

  Active(active) {
    console.log("active", active)
    this.props.navigation.navigate(active)

  }

  render() {
    const routeName = this.props.navigation.state.routeName;
    return (
      <Footer style={{ backgroundColor: 'transparent', margin: 20, height: 75, elevation: 0, borderColor: 'transparent', borderWidth: 0 }}>
        <View style={styles.ShapeOneRow}>

          {routeName === 'CPR' || routeName === 'CPR_Detail' ? (
            <View style={styles.leftShapeActive} >
              <View style={styles.leftShapeActive01}>
                  <View style={styles.commonShapeActive1} />
                  <View style={styles.commonShapeActive2} />
                  <View style={styles.commonShapeActive3} />
                  <View style={styles.commonShapeActive4} />
                  <View style={styles.commonShapeActive5} />
                  <View style={styles.commonShapeActive6} />
                  <View style={styles.commonShapeActive7} />
                  <View style={styles.commonShapeActive8} />
                  <View style={styles.commonShapeActive9} />
                  <View style={styles.commonShapeActive10} />
                  <View style={styles.commonShapeActive11} />
                  <View style={styles.commonShapeActive12} />
                  <View style={styles.commonShapeActive13} />
                  <View style={styles.commonShapeActive14} />
                  <View style={styles.commonShapeActive15} />
                  <View style={styles.commonShapeActive16} />
                  <View style={styles.commonShapeActive17} />
                  <View style={styles.commonShapeActive18} />
                  <View style={styles.commonShapeActive19} />
                  <View style={styles.commonShapeActive20} />
                  <View style={styles.commonShapeActive21} />
                  <View style={styles.commonShapeActive22} />
                  <View style={styles.commonShapeActive23} />
                  <View style={styles.commonShapeActive24} />
                  <View style={styles.commonShapeActive25} />
              </View>
              <Button style={styles.buttonActivetwo} >
                <Icon style={{ alignSelf: 'center', color: '#D7253B', fontSize: 32, borderWidth: 0, padding: 0 }} name="medkit" type='FontAwesome' />
              </Button>
            </View>) :
            <View style={styles.ShapeOne} >
              <Button transparent style={styles.shapeOneButton} onPress={() => { this.Active("CPR") }}>
                <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 30, borderWidth: 0, padding: 0 }} name="medkit" type='FontAwesome' />
              </Button>
            </View>
          }
          {/* Center point */}
          {routeName === 'Home' || routeName === 'MapFinder' ? (
            <View style={styles.centerShapeActive} >
              <View style={styles.centerShapeActive01}>
              <View style={styles.commonShapeActive1} />
                  <View style={styles.commonShapeActive2} />
                  <View style={styles.commonShapeActive3} />
                  <View style={styles.commonShapeActive4} />
                  <View style={styles.commonShapeActive5} />
                  <View style={styles.commonShapeActive6} />
                  <View style={styles.commonShapeActive7} />
                  <View style={styles.commonShapeActive8} />
                  <View style={styles.commonShapeActive9} />
                  <View style={styles.commonShapeActive10} />
                  <View style={styles.commonShapeActive11} />
                  <View style={styles.commonShapeActive12} />
                  <View style={styles.commonShapeActive13} />
                  <View style={styles.commonShapeActive14} />
                  <View style={styles.commonShapeActive15} />
                  <View style={styles.commonShapeActive16} />
                  <View style={styles.commonShapeActive17} />
                  <View style={styles.commonShapeActive18} />
                  <View style={styles.commonShapeActive19} />
                  <View style={styles.commonShapeActive20} />
                  <View style={styles.commonShapeActive21} />
                  <View style={styles.commonShapeActive22} />
                  <View style={styles.commonShapeActive23} />
                  <View style={styles.commonShapeActive24} />
                  <View style={styles.commonShapeActive25} />
              </View>
              <Button style={styles.buttonActivetwo}>
                {/* <Icon style={{ alignSelf: 'center', color: '#D7253B', fontSize: 32, borderWidth: 0, padding: 0 }} name="heart" type='FontAwesome' /> */}
             <Image source={require('../assets/Images/HEART1/HEART-GIG/ht0.03.gif')} style={{width:55,height:40,borderWidth: 0, padding: 0,marginLeft:5}}/>
             </Button>
            </View>
            ) :
            <View style={styles.ShapeTwo} >
              <Button transparent style={styles.ShapeTwoButton} onPress={() => { this.Active("Home") }}>
                <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 30, borderWidth: 0, padding: 0 }} name="heart" type='FontAwesome' />
              </Button>
            </View>
          }

          {routeName === 'ActiveOnMap' ? (

            <View style={styles.rightShapeActive} >
              <View style={styles.rightShapeActive01}>
              <View style={styles.commonShapeActive1} />
                  <View style={styles.commonShapeActive2} />
                  <View style={styles.commonShapeActive3} />
                  <View style={styles.commonShapeActive4} />
                  <View style={styles.commonShapeActive5} />
                  <View style={styles.commonShapeActive6} />
                  <View style={styles.commonShapeActive7} />
                  <View style={styles.commonShapeActive8} />
                  <View style={styles.commonShapeActive9} />
                  <View style={styles.commonShapeActive10} />
                  <View style={styles.commonShapeActive11} />
                  <View style={styles.commonShapeActive12} />
                  <View style={styles.commonShapeActive13} />
                  <View style={styles.commonShapeActive14} />
                  <View style={styles.commonShapeActive15} />
                  <View style={styles.commonShapeActive16} />
                  <View style={styles.commonShapeActive17} />
                  <View style={styles.commonShapeActive18} />
                  <View style={styles.commonShapeActive19} />
                  <View style={styles.commonShapeActive20} />
                  <View style={styles.commonShapeActive21} />
                  <View style={styles.commonShapeActive22} />
                  <View style={styles.commonShapeActive23} />
                  <View style={styles.commonShapeActive24} />
                  <View style={styles.commonShapeActive25} />
              </View>
              <Button style={styles.buttonActivetwo}>
                <Icon style={{ alignSelf: 'center', color: '#D7253B', fontSize: 32, borderWidth: 0, padding: 0 }} name="map" type='FontAwesome' />
              </Button>
            </View>) :
            <View style={styles.ShapeThree} >
              <Button transparent style={styles.ShapeThreeButton} onPress={() => { this.Active("ActiveOnMap") }}>
                <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 30, borderWidth: 0, padding: 0 }} name="map" type='FontAwesome' />
              </Button>
            </View>
          }

        </View>
      </Footer>
      // <Footer style={{ backgroundColor: 'transparent', margin: 20, height: 75, elevation: 0, borderColor: 'transparent', borderWidth: 0 }}>

      //   {routeName === 'Home' || routeName === 'MapFinder' ? (
      //     <View style={{ width: '100%', position: 'relative' }}>
      //       <View style={styles.footerCommanShape} >
      //         <View style={styles.middelcircle1}></View>
      //         <View style={styles.dot1}></View>
      //         <View style={styles.dot2}></View>
      //         <View style={styles.dot3}></View>
      //         <View style={styles.dot4}></View>
      //       </View>
      //       <Button style={styles.middelcircleIcon1} onPress={() => {this.Active("CPR")}}>
      //         <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 24, borderWidth: 0, padding: 0 }} name="medkit" type='FontAwesome' />
      //       </Button>
      //       <Button style={styles.middelcircleIcon2} onPress={() => {this.Active("Home")}}>
      //         <Icon style={{ alignSelf: 'center', color: '#D7253B', fontSize: 21, borderWidth: 0 }} name="heartbeat" type='FontAwesome' />
      //       </Button>
      //       <Button onPress={() => this.Active("ActiveOnMap")} style={styles.middelcircleIcon3}>
      //         <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 24, borderWidth: 0 }} name="map" type='FontAwesome' />
      //       </Button>
      //     </View>
      //   ) : null}

      //   {routeName === 'CPR' || routeName === 'CPR_Detail' ? (
      //     <View style={{ width: '100%', position: 'relative' }}>
      //       <View style={styles.footerCommanShape} >
      //         <View style={styles.middelcircle1Left}></View>
      //         <View style={styles.dot1Left}></View>
      //         <View style={styles.dot2Left}></View>
      //         <View style={styles.dot3}></View>
      //         <View style={styles.dot4Left}></View>
      //       </View>
      //       <Button style={styles.middelcircleIcon1Left} onPress={() => {this.Active("CPR")}}>
      //         <Icon style={{ alignSelf: 'center', color: '#D7253B', fontSize: 21, borderWidth: 0, padding: 0 }} name="medkit" type='FontAwesome' />
      //       </Button>
      //       <Button style={styles.middelcircleIcon2Left} onPress={() => {this.Active("Home")}}>
      //         <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 26, borderWidth: 0 }} name="heartbeat" type='FontAwesome' />
      //       </Button>
      //       <Button onPress={() => this.Active("ActiveOnMap")} style={styles.middelcircleIcon3}>
      //         <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 24, borderWidth: 0 }} name="map" type='FontAwesome' />
      //       </Button>
      //     </View>
      //   ) : null}


      //   {routeName === 'ActiveOnMap' ? (
      //     <View style={{ width: '100%', position: 'relative' }}>
      //       <View style={styles.footerCommanShape} >
      //         <View style={styles.middelcircle1Right}></View>
      //         <View style={styles.dot1Right}></View>
      //         <View style={styles.dot2Right}></View>
      //         <View style={styles.dot3}></View>
      //         <View style={styles.dot4Right}></View>
      //       </View>
      //       <Button style={styles.middelcircleIcon1} onPress={() => {this.Active("CPR")}}>
      //         <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 24, borderWidth: 0, padding: 0 }} name="medkit" type='FontAwesome' />
      //       </Button>
      //       <Button style={styles.middelcircleIcon2Right} onPress={() => {this.Active("Home")}}>
      //         <Icon style={{ alignSelf: 'center', color: '#5f697a', fontSize: 26, borderWidth: 0 }} name="heartbeat" type='FontAwesome' />
      //       </Button>
      //       <Button onPress={() => {this.Active("ActiveOnMap")}} style={styles.middelcircleIcon3Right}>
      //         <Icon style={{ alignSelf: 'center', color: '#D7253B', fontSize: 20, borderWidth: 0 }} name="map" type='FontAwesome' />
      //       </Button>
      //     </View>
      //   ) : null}


      // </Footer>
    );
  }
}

const styles = StyleSheet.create({
  ShapeOneRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3
  },
  ShapeOne: {
    borderTopStartRadius: 20,
    borderBottomStartRadius: 40,
    borderBottomWidth: 2,
    borderLeftWidth: 1,
    borderColor: '#eee',
    width: '33%',
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 75,
  },
  shapeOneButton: {
    backgroundColor: 'transparent',
    height: 65,
    width: 65,
    elevation: null,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center'
  },
  ShapeTwo: {
    borderBottomWidth: 2,
    borderColor: '#eee',
    width: '33%',
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 75,
  },
  ShapeTwoButton: {
    backgroundColor: 'transparent',
    height: 65,
    width: 65,
    elevation: null,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center'
  },
  ShapeThree: {
    borderTopEndRadius: 20,
    borderBottomEndRadius: 40,
    borderBottomWidth: 2,
    borderColor: '#eee',
    width: '33%',
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 75,
  },
  ShapeThreeButton: {
    backgroundColor: 'transparent',
    height: 65,
    width: 65,
    elevation: null,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center'
  },
  shapeThreeButton: {
    backgroundColor: 'transparent',
    height: 65,
    width: 65,
    elevation: null,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center'
  },
  // ShapeTwo:{
  //   borderBottomWidth: 2,
  //   borderColor: '#eee',
  //   width: '33%', 
  //   backgroundColor: '#fff',
  //   flexDirection: 'row',
  //   alignItems:'center',
  //   justifyContent:'center',
  //   height: 75,
  // },
  leftShapeActive: {
    width: '33.333%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    position: 'relative',
  },
  leftShapeActive01: {
    width: '100%',
    height: 75,
    overflow: 'hidden',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
    borderTopStartRadius: 20,
    borderBottomStartRadius: 40,
    borderBottomWidth: 2,
    borderLeftWidth: 1,
    borderColor: '#eee',
  },
  // leftShapeActive1: {
  //   borderWidth: 4,
  //   // borderBottomWidth: 8,
  //   // borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'relative',
  //   top: -25,
  //   width: 92,
  //   height: 90,
  // },
  // leftShapeActive2: {
  //   borderWidth: 8,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -30,
  //   width: 100,
  //   height: 100,
  // },
  // leftShapeActive3: {
  //   borderWidth: 15,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -40,
  //   width: 120,
  //   height: 120,
  // },
  // leftShapeActive4: {
  //   borderWidth: 20,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -50,
  //   width: 140,
  //   height: 140,
  // },
  // leftShapeActive5: {
  //   borderWidth: 25,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -60,
  //   width: 160,
  //   height: 160,
  // },
  // leftShapeActive6: {
  //   borderWidth: 80,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -100,
  //   width: 300,
  //   height: 300,
  // },
  // leftShapeActive7: {
  //   borderWidth: 100,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -240,
  //   width: 640,
  //   height: 640,
  // },
  // leftShapeActive8: {
  //   borderWidth: 100,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -240,
  //   width: 640,
  //   height: 640,
  // },

  centerShapeActive: {
    width: '33.333%',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    borderBottomWidth: 2,
    borderColor: '#eee'
  },
  centerShapeActive01: {
    backgroundColor: 'transparent',
    width: '100%',
    height: 75,
    overflow: 'hidden',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
  },
  // centerShapeActive1: {
  //   borderWidth: 4,
  //   borderBottomWidth: 8,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'relative',
  //   top: -25,
  //   width: 92,
  //   height: 90,
  // },
  // centerShapeActive2: {
  //   borderWidth: 8,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'absolute',
  //   top: -30,
  //   width: 100,
  //   height: 100,
  // },
  // centerShapeActive3: {
  //   borderWidth: 15,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'absolute',
  //   top: -40,
  //   width: 120,
  //   height: 120,
  // },
  // centerShapeActive4: {
  //   borderWidth: 20,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'absolute',
  //   top: -50,
  //   width: 140,
  //   height: 140,
  // },
  // centerShapeActive5: {
  //   borderWidth: 25,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'absolute',
  //   top: -60,
  //   width: 160,
  //   height: 160,
  // },
  // centerShapeActive6: {
  //   borderWidth: 80,
  //   borderRadius: 50,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -100,
  //   width: 300,
  //   height: 300,
  // },
  // centerShapeActive7: {
  //   borderWidth: 100,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'absolute',
  //   top: -240,
  //   width: 640,
  //   height: 640,
  // },
  // centerShapeActive8: {
  //   borderWidth: 100,
  //   borderRadius: 50,
  //   borderColor: 'white',
  //   position: 'absolute',
  //   top: -240,
  //   width: 640,
  //   height: 640,
  // },
  rightShapeActive: {
    width: '33.333%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    position: 'relative',
  },
  rightShapeActive01: {
    width: '100%',
    height: 75,
    overflow: 'hidden',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
    borderTopEndRadius: 20,
    borderBottomEndRadius: 40,
    borderBottomWidth: 2,
    borderRightWidth: 1,
    borderColor: '#eee',
  },
  commonShapeActive1: {
    borderWidth: 1,
    borderRadius: 90,
    borderColor: '#f8f9f9',
    position: 'absolute',
    top: -30,
    width: 90,
    height: 90,
  }
  ,
  commonShapeActive2: {
    borderWidth: 7,
    borderRadius: 100,
    borderColor: '#fff',
    position: 'absolute',
    top: -35,
    width: 100,
    height: 100,
  }
  ,
  commonShapeActive3: {
    borderWidth: 7,
    borderRadius: 110,
    borderColor: '#fff',
    position: 'absolute',
    top: -40,
    width: 110,
    height: 110,
  }
  ,
  commonShapeActive4: {
    borderWidth: 7,
    borderRadius: 120,
    borderColor: '#fff',
    position: 'absolute',
    top: -45,
    width: 120,
    height: 120,
  }
  ,
  commonShapeActive5: {
    borderWidth: 6,
    borderRadius: 130,
    borderColor: '#fff',
    position: 'absolute',
    top: -50,
    width: 130,
    height: 130,
  }
  ,
  commonShapeActive6: {
    borderWidth: 6,
    borderRadius: 140,
    borderColor: '#fff',
    position: 'absolute',
    top: -55,
    width: 140,
    height: 140,
  },
  commonShapeActive7: {
    borderWidth: 6,
    borderRadius: 150,
    borderColor: '#fff',
    position: 'absolute',
    top: -60,
    width: 150,
    height: 150,
  },
  commonShapeActive8: {
    borderWidth: 6,
    borderRadius: 150,
    borderColor: '#fff',
    position: 'absolute',
    top: -60,
    width: 150,
    height: 150,
  },
  commonShapeActive9: {
    borderWidth: 8,
    borderRadius: 160,
    borderColor: '#fff',
    position: 'absolute',
    top: -65,
    width: 160,
    height: 160,
  },
  commonShapeActive10: {
    borderWidth: 6,
    borderRadius: 170,
    borderColor: '#fff',
    position: 'absolute',
    top: -70,
    width: 170,
    height: 170,
  },
  commonShapeActive11: {
    borderWidth: 6,
    borderRadius: 180,
    borderColor: '#fff',
    position: 'absolute',
    top: -75,
    width: 180,
    height: 180,
  },
  commonShapeActive12: {
    borderWidth: 6,
    borderRadius: 190,
    borderColor: '#fff',
    position: 'absolute',
    top: -80,
    width: 190,
    height: 190,
  },
  commonShapeActive13: {
    borderWidth: 6,
    borderRadius: 200,
    borderColor: '#fff',
    position: 'absolute',
    top: -85,
    width: 200,
    height: 200,
  },
  commonShapeActive14: {
    borderWidth: 8,
    borderRadius: 210,
    borderColor: '#fff',
    position: 'absolute',
    top: -90,
    width: 210,
    height: 210,
  },
  commonShapeActive15: {
    borderWidth: 8,
    borderRadius: 220,
    borderColor: '#fff',
    position: 'absolute',
    top: -95,
    width: 220,
    height: 220,
  },
  commonShapeActive16: {
    borderWidth: 8,
    borderRadius: 230,
    borderColor: '#fff',
    position: 'absolute',
    top: -100,
    width: 230,
    height: 230,
  },
  commonShapeActive17: {
    borderWidth: 8,
    borderRadius: 240,
    borderColor: '#fff',
    position: 'absolute',
    top: -105,
    width: 240,
    height: 240,
  },
  commonShapeActive18: {
    borderWidth: 8,
    borderRadius: 250,
    borderColor: '#fff',
    position: 'absolute',
    top: -110,
    width: 250,
    height: 250,
  },
  commonShapeActive19: {
    borderWidth: 8,
    borderRadius: 260,
    borderColor: '#fff',
    position: 'absolute',
    top: -115,
    width: 260,
    height: 260,
  },
  commonShapeActive20: {
    borderWidth: 8,
    borderRadius: 270,
    borderColor: '#fff',
    position: 'absolute',
    top: -120,
    width: 270,
    height: 270,
  },
  commonShapeActive21: {
    borderWidth: 12,
    borderRadius: 285,
    borderColor: '#fff',
    position: 'absolute',
    top: -130,
    width: 285,
    height: 285,
  },
  commonShapeActive22: {
    borderWidth: 14,
    borderRadius: 300,
    borderColor: '#fff',
    position: 'absolute',
    top: -135,
    width: 300,
    height: 300,
  },
  commonShapeActive23: {
    borderWidth: 28,
    borderRadius: 350,
    borderColor: '#fff',
    position: 'absolute',
    top: -157,
    width: 350,
    height: 350,
  },
  commonShapeActive24: {
    borderWidth: 40,
    borderRadius: 400,
    borderColor: '#fff',
    position: 'absolute',
    top: -185,
    width: 400,
    height: 400,
  },
  commonShapeActive25: {
    borderWidth: 40,
    borderRadius: 400,
    borderColor: '#fff',
    position: 'absolute',
    top: -210,
    width: 450,
    height: 450,
  },
  // centerShapeActive8:{
  //   borderLeftWidth: 60,
  //   borderRightWidth: 60,
  //   borderColor: '#fff',
  //   position: 'absolute',
  //   top: -30,
  //   width: '100%',
  //   height: 200,
  // },
  buttonActivetwo: {
    backgroundColor: '#fff',
    borderRadius: 50,
    height: 65,
    width: 65,
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    top: -20,
    elevation: 5,
    marginLeft: 5,
  },
  footerCommanShape: {
    position: 'relative',
    overflow: 'hidden',
    borderBottomEndRadius: 40,
    borderBottomStartRadius: 40,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    height: 70,
    width: '100%',
    borderBottomWidth: 2,
    borderColor: '#eee',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    shadowColor: '#000000',
    shadowOffset: {
      width: 10,
      height: 25
    },
    shadowRadius: 5,
    shadowOpacity: 5
  },
  middelcircle1: {
    position: 'absolute',
    right: 0,
    left: '50%',
    marginLeft: -50,
    textAlign: 'center',
    margin: 'auto',
    borderWidth: 18,
    borderColor: '#fff',
    width: 114,
    height: 114,
    borderRadius: 100,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -48
  },
  middelcircle1Left: {
    position: 'absolute',
    right: 0,
    left: '23%',
    marginLeft: -50,
    textAlign: 'center',
    margin: 'auto',
    borderWidth: 18,
    width: 114,
    height: 114,
    borderRadius: 100,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -48
  },
  middelcircle1Right: {
    position: 'absolute',
    right: '3%',
    marginLeft: -50,
    textAlign: 'center',
    margin: 'auto',
    borderWidth: 18,
    width: 114,
    height: 114,
    borderRadius: 100,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -48
  },
  dot1: {
    width: '50%',
    position: 'absolute',
    left: -40,
    borderRadius: 0,
    color: '#fff',
    backgroundColor: '#fff',
    height: ' 100%',
    zIndex: -1,
  },
  dot1Left: {
    width: '10%',
    position: 'absolute',
    left: 0,
    borderRadius: 0,
    color: '#fff',
    backgroundColor: '#fff',
    height: ' 100%',
    zIndex: -1,
  },
  dot1Right: {
    width: '67%',
    position: 'absolute',
    left: 0,
    borderRadius: 0,
    color: '#fff',
    backgroundColor: '#fff',
    height: ' 100%',
    zIndex: -1,
  },
  dot2: {
    width: '50%',
    borderRadius: 0,
    right: -52,
    position: 'absolute',
    height: '100%',
    backgroundColor: '#fff',
    zIndex: -1,
  },
  dot2Left: {
    width: '60%',
    borderRadius: 0,
    right: 0,
    position: 'absolute',
    height: '100%',
    backgroundColor: '#fff',
    zIndex: -1,
  },
  dot2Right: {
    width: '7%',
    borderRadius: 0,
    right: 0,
    position: 'absolute',
    height: '100%',
    backgroundColor: '#FFF',
    zIndex: -1,
  },
  dot3: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: 20,
    borderRadius: 0,
    backgroundColor: '#fff',
    zIndex: -1,
  },
  dot4: {
    position: 'absolute',
    right: 0,
    left: '50%',
    marginLeft: -50,
    textAlign: 'center',
    margin: 'auto',
    borderWidth: 18,
    borderColor: '#fff',
    width: 120,
    height: 120,
    borderRadius: 100,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -40,
    zIndex: -1,
  },
  dot4Left: {
    position: 'absolute',
    right: 0,
    left: '22%',
    marginLeft: -50,
    textAlign: 'center',
    margin: 'auto',
    borderWidth: 18,
    width: 120,
    height: 120,
    borderRadius: 100,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -40,
    zIndex: -1,
  },
  dot4Right: {
    position: 'absolute',
    right: '3%',
    textAlign: 'center',
    margin: 'auto',
    borderWidth: 18,
    width: 120,
    height: 120,
    borderRadius: 100,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -40,
    zIndex: -1,
  },

  // Default Blank Button
  middelcircleIcon1: {
    position: 'absolute',
    top: 10,
    left: '10%',
    marginLeft: 0,
    borderColor: 'transparent',
    backgroundColor: '#fff',
    borderColor: '#fff',
    zIndex: 999,
    elevation: 0,

  },
  middelcircleIcon1Left: {
    position: 'absolute',
    top: -17,
    left: '18%',
    marginLeft: 0,
    height: 55,
    width: 55,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderRadius: 100,
    zIndex: 999,
    borderWidth: 1,
    borderColor: '#F4F4F4'
  },

  // Active Button
  middelcircleIcon2: {
    position: 'absolute',
    right: 0,
    left: '50%',
    marginLeft: -21,
    textAlign: 'center',
    margin: 'auto',
    width: 56,
    height: 56,
    borderRadius: 50,
    // borderColor: '#fff',
    borderTopColor: 'transparent',
    top: -17,
    backgroundColor: '#fff',
    alignSelf: 'center',
    padding: 0,
    borderWidth: 1,
    borderColor: '#F4F4F4'
  },
  middelcircleIcon2Left: {
    position: 'absolute',
    right: 0,
    left: '50%',
    margin: 'auto',
    top: 10,
    marginLeft: -18,
    textAlign: 'center',
    borderRadius: 50,
    borderColor: '#fff',
    borderTopColor: 'transparent',
    backgroundColor: 'transparent',
    alignSelf: 'center',
    padding: 0,
    zIndex: 999,
    elevation: 0,
  },
  middelcircleIcon2Right: {
    position: 'absolute',
    left: '50%',
    margin: 'auto',
    top: 10,
    marginLeft: -25,
    textAlign: 'center',
    backgroundColor: 'transparent',
    alignSelf: 'center',
    padding: 0,
    elevation: 0,
  },
  // Default Blank Button
  middelcircleIcon3: {
    position: 'absolute',
    top: 10,
    right: '10%',
    margin: 'auto',
    marginLeft: 0,
    backgroundColor: '#fff',
    textAlign: 'center',
    alignSelf: 'center',
    borderRadius: 50,
    borderColor: '#fff',
    zIndex: 999,
    elevation: 0,
    padding: 0
  },
  middelcircleIcon3Right: {
    position: 'absolute',
    top: -13,
    right: '12%',
    margin: 'auto',
    marginLeft: 0,
    backgroundColor: '#fff',
    textAlign: 'center',
    alignSelf: 'center',
    borderRadius: 50,
    width: 50,
    height: 50,
    //borderColor: '#fff',
    borderWidth: 1,
    borderColor: '#F4F4F4'
  },


})