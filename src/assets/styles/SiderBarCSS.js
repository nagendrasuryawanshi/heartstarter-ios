const Styles = {
    content: {
        borderRightColor: "rgb(215, 37, 59)",
        borderRightWidth: 2,
        backgroundColor: "#fff"
    },

    greyColor: {
        color: "rgb(93, 113, 131)"
    },
    redColor: {
        color: "rgb(215, 37, 59)"
    },
    thumbnail: {
        backgroundColor: "rgb(215, 37, 59)"
    },
    bodyText: {
        color: "rgb(215, 37, 59)"
    },
    button: {
        backgroundColor: "red",
        borderRadius: 35
    },
    title: {
        color: "#000",
        marginRight: "5%"
    },
    text: {
        color: "red"
    }
}

export default Styles;