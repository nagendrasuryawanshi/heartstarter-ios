import { Dimensions } from "react-native";
const screen = Dimensions.get("window");
const Styles = {
    footer: {
        margin: 10,
        backgroundColor: "#fff",
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    dot: {
        height: 65,
        width: 65,
        backgroundColor: "rgb(249, 249, 249)",
        borderRadius: 50,
        marginTop: "-10%",
        alignSelf: "center"
    },

    autoScroll: {
        overflow: "scroll",
        marginTop: 10
    },
    playColor: {
        fontSize: 40,
        color: "rgb(215, 37, 59)"
    },
    stopColor: {
        fontSize: 40,
        color: "rgb(93, 113, 131)"
    },
    container: {
        backgroundColor: "#f9f9f9",

    },
    view: {
        //marginTop: 100,
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 10,
        paddingTop: screen.height / 5,
        top: -5,

    },
    viewText: {
        alignSelf: "center"
    },
    viewCard: {
        backgroundColor: "#fff",
        marginTop: 5,
        shadowColor: "#ebebeb",
        borderRadius: 10,
        overflow: 'hidden'
    },
    viewRow: {
        backgroundColor: "rgb(215, 37, 59)",
        marginBottom: 10,
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        // height: screen.height-250*3
    },
    viewBody: {
        color: "#fff",
        fontSize: 40
    },
    viewBodyText: {
        color: "#fff",
        // marginTop: -5
    },
    ViewIcon: {
        color: "#fff",
        marginTop: '10%',
        marginLeft: "-12%",
        marginRight: 10
    },
    scrollView: {
        marginTop: 10
    },
    scrollViewCardItem: {
        // alignSelf: "center",
        height: screen.height - 160,
    },
    scrollViewCardItem01: {
        alignSelf: "center",
        height: screen.height - 160,
    },
    image: {
        height: 100,
        width: 100,
        alignSelf: "center"
    },
    mapRow: {
        marginTop: 2
    },
    mapCol: {
        width: "20%"
    },
    mapBadge: {
        width: 27
    },
    mapText: {
        color: "#fff",
        //alignSelf: "center",
        marginTop: 2,
        marginLeft: 6,
    },
    icon: {
        alignSelf: "center",
        fontSize: 40,
        color: "#dcdcdc"
    },
    footerCard: {
        backgroundColor: "#fff",
        width: 45,
        borderRadius: 50,
        height: 45,
        marginLeft: 10,
        marginTop: 10
    },
    footerIcon: {
        color: "rgb(215, 37, 59)",
        alignItems: "center",
        margin: 7
    },
    footerIconFirst: {
        color: "rgb(93, 113, 131)"
    },
    modalColor: {
        backgroundColor: "rgba(0,0,0,0.4)"
    },
    modalCard: {
        marginTop: "30%",
        marginLeft: 50,
        marginRight: 50
    },
    modalIcon: {
        color: "rgb(215, 37, 59)",
        fontSize: 20
    },
    modalCardItem: {
        color: 'rgb(215, 37, 59)',
        fontSize: 50,
        alignSelf: 'center'
    }
}
export default Styles;
